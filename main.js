let dropDown = document.getElementById("drop-down-icon");
let checkbox = document.querySelector(".btn input");
checkbox.style.display = "none";
checkbox.addEventListener("click", function (e) {
    let navBar = document.getElementById("header-list-elements");
    if (checkbox.checked === true) {
        navBar.style.position = "absolute";
        navBar.style.display = "grid";
        navBar.style.width = '85%';
        navBar.style.listStyle = 'none';
        navBar.style.gridGap = "1.5em";
        navBar.style.textAlign = 'center';
        navBar.style.borderRadius = "5px";
        navBar.style.boxSizing = "border-box";
        navBar.style.padding = "1.5em";
        navBar.style.backgroundColor = "white";
        dropDown.src = "./images/icon-close.svg";
    } else {
        dropDown.src = "./images/icon-hamburger.svg";
        navBar.style.display = 'none';
    }
});